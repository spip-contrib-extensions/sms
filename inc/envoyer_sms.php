<?php
/**
 * Envoyer un SMS,
 *   via un job :
 *     ```
 *     $id_job = job_queue_add(
 *       'envoyer_sms',
 *       $description,
 *       array($message,$dest,$options),
 *       'inc/',
 *       true,
 *       $time
 *     );
 *     ```
 *   via la fonction charger_fonction()
 *     ```
 *     $envoyer_sms = charger_fonction('envoyer_sms', 'inc');
 *     $envoyer_sms($message, $dest, $options);
 *     ```
 * @plugin	   SMS SPIP
 * @copyright  2015
 * @author	   tofulm
 * @licence    GNU/GPL
 * @package    SPIP\Sms\Inc\Envoyer_sms
 */

// sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

include_spip('inc/config');

function inc_envoyer_sms_dist($message,$destinataire,$options=[]) {
    if (lire_config('sms/prestataire') == "smsfactor") {
    	return smsfactor($message,$destinataire,$options);
    } elseif (lire_config('sms/prestataire') == "octopush") {
        return octopush($message,$destinataire,$options);
    } else {
    	return false;
    }
}

/**
 * Envoie le sms en utilisant l'API du prestataire sms factor
 *
 * @param string $message
 *		le texte d'envoie doit etre du texte et non du html
 * @param array $destinataire
 * @param array $arg
 *		utilise pour : $options['sender']
 * @return boolean
**/
function smsfactor($message,$destinataire,$options = []) {
	$username = lire_config('sms/login_smsfactor');
	$password = lire_config('sms/mdp_smsfactor');
	$options['sender'] = $options['sender'] ?? lire_config('sms/expediteur_smsfactor');
	$options['token'] = $options['token'] ?? lire_config('sms/token_smsfactor');
	$options['type_sms'] = $options['type_sms'] ?? '';
	include_spip('inc/filtres');
	$message  = textebrut($message);

	include_spip('classes/smsfactor/sendSMSclass');
	$SENDSMS = new SendSMSclass();
	if (defined('_TEST_SMS_DEST') and _TEST_SMS_DEST) {
		if (!is_array(_TEST_SMS_DEST)) {
			$destinataire = [_TEST_SMS_DEST];
		} else {
			$destinataire = _TEST_SMS_DEST;
		}
	}

	$reponse  = $SENDSMS->SendSMS($username,$password,$options,$message,$destinataire);
	$reponse = json_decode($reponse);

	if ( $reponse->message === "OK" ) {
		if (count($destinataire)) {
			$cost = $reponse->cost;
			$sent = $reponse->sent;
			$nbr_sms = 0;
			if ($sent != 0) {
				$nbr_sms = $cost / $sent;
			}

			$type_sms = '';
			if (array_key_exists('type_sms', $options)) {
				$type_sms = $options['type_sms'];
			}

			foreach ($destinataire as $tel) {
				$set = [
					'telephone' => md5($tel),
					'date'      => date("Y-m-d H:i:s"),
					'message'   => $message,
					'nbr_sms'   => $nbr_sms,
					'type_sms'  => $type_sms
				];
				sql_insertq('spip_sms_logs',$set);
			}
		}
		return true;
	} else {
		spip_log('Le message ' . $message . ' pour ' . implode(',', $destinataire) . ' à pour retour : ' . print_r($reponse,true), 'sms_error');
		return false;
	}

}

/**
 * Envoie le sms en utilisant l'API du prestataire octopush-dm
 *
 * @param string $message
 *		le texte d'envoie doit etre du texte et non du html
 * @param array $destinataire
 * @param array $arg
 *		utilise pour : $arg['sms_sender']
 *		utilise pour : $arg['sms_mode'] => XXX = LowCost; FR = Premium; WWW = Monde
 *		utilise pour : $arg['sms_type'] => INSTANTANE (par defaut) ou DIFFERE (Non prévu pour le moment)
 * @return boolean
**/
function octopush($sms_text,$sms_recipients,$arg) {
	$user_login	= lire_config('sms/login_octopush');
	$api_key	= lire_config('sms/cle_api_octopush');
	$sms_text	= nettoyer_xml($sms_text);

	// Variable pour l'envoi
	$sms_type	= isset($arg['sms_type']) ? $arg['sms_type'] : 'XXX';
	$sms_mode	= isset($arg['sms_mode']) ? $arg['sms_mode'] : 'INSTANTANE';
	$sms_sender	= isset($arg['sms_sender']) ? $arg['sms_sender'] : lire_config('sms/expediteur_octopush');
	include_spip('classes/octopush/sms.inc');

	if (_TEST_SMS_DEST) {
		if (!is_array(_TEST_SMS_DEST)) {
			$destinataire = [_TEST_SMS_DEST];
		} else {
			$sms_recipients = _TEST_SMS_DEST;
		}
	}
	$sms = new SMS_OCTOSPUSH();

	$sms->set_user_login($user_login);
	$sms->set_api_key($api_key);
	$sms->set_sms_mode($sms_mode);
	$sms->set_sms_text($sms_text);
	$sms->set_sms_recipients($sms_recipients);
	$sms->set_sms_type($sms_type);
	$sms->set_sms_sender($sms_sender);
	$sms->set_sms_request_id(uniqid());
	$sms->set_option_with_replies(0);
	$sms->set_option_transactional(1);
	$sms->set_sender_is_msisdn(0);
	//$sms->set_date(2016, 4, 17, 10, 19); // En cas d'envoi différé.
	$sms->set_request_keys('TRS');
	$xml = $sms->send();
	$xml = simplexml_load_string($xml);
	// Enregistrement pour suivi
	foreach ($sms_recipients as $tel) {
		$set = [
			'telephone' => md5($tel),
			'date'      => date("Y-m-d H:i:s"),
			'message'   => $xml->error_code,
			'nbr_sms'   => '',
			'type_sms'  => $sms_type
		];
		sql_insertq('spip_sms_logs',$set);
	}
	return $xml;
}


