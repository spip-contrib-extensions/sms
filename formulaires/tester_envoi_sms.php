<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
function formulaires_tester_envoi_sms_charger_dist($redirect = '') {
	$destinataire = [];
	if (defined('_TEST_SMS_DEST') and _TEST_SMS_DEST) {
		if (!is_array(_TEST_SMS_DEST)) {
			$destinataire[] = _TEST_SMS_DEST;
		} else {
			$destinataire = _TEST_SMS_DEST;
		}
	}
	$valeurs = array(
		'tel'     => $destinataire[0] ?? '',
		'message' => _T('sms:demo_sms_test'),
	);

	return $valeurs;
}
function formulaires_tester_envoi_sms_verifier_dist($redirect = '') {
	$erreurs  = array();
	$tel     = [_request('tel')];
	$message = _request('message');
	$mode = _request('mode');
	$options['type_sms'] = (_request('simulate') == 'on') ? 'simulate' : 'test';
	include_spip('inc/config');
	$prestataire = lire_config('sms/prestataire');
	// envoi SMS via sms_fonctions.php => envoyer_sms()
	if ($mode === 'direct'){
		$r = envoyer_sms($message . " (envoyer_sms $prestataire)", $tel, $options );
		if (!$r) {
			$erreurs['via_appel_direct'] = 'error';
		} else {
			$erreurs['via_appel_direct'] = 'envoye';
		}
	}
	// envoi SMS via charger_fonction() => envoyer_sms()
	if ($mode === 'charger'){
		$r = false;
		if ($envoyer_sms = charger_fonction('envoyer_sms', 'inc', true)) {
			$r = $envoyer_sms($message . " (charger_fonction $prestataire)", $tel, $options );
		}
		if (!$r) {
			$erreurs['via_appel_charger_fonction'] = 'error';
		} else {
			$erreurs['via_appel_charger_fonction'] = 'envoye';
		}
	}
	return $erreurs;
}

