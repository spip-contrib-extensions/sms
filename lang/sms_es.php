<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/sms?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_exemple' => 'Ejemplo',
	'cfg_exemple_explication' => 'Explicación del ejemplo',
	'cfg_titre_parametrages' => 'Ajustes',
	'choix_prestataire' => 'Elección de proveedor',

	// E
	'expediteur' => 'Remitente',
	'explication_expediteur' => 'Personalización del remitente del texto (11 caracteres como máximo)
N° de teléfono sin caraga para Francia.', # MODIF

	// M
	'mot_de_passe' => 'Contraseña',

	// T
	'titre_page_configurer_sms' => 'Configuración de la API SMS'
);
