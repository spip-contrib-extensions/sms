<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/sms.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sms_description' => 'API pour envoyer des sms simplement',
	'sms_nom' => 'SMS SPIP',
	'sms_slogan' => ''
);
