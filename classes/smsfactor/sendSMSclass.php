<?php




class SendSMSclass
{
	//input parameters ----------------
	var $username;                     //your username
	var $password;                     //your password
	var $option = [];                  // sender...
	var $message;                      //message text
	var $inputgsmnumbers = [];        //destination gsm numbers
	// variables ne servant que dans la classe, on met leurs propriétés en privé.
	private string $host;
	private string $token;
	private string $sender;
	private array $optional_headers;
	private array $fomatedGSM = [];
	private array $formatedMessage = [];
	private string $request_data;
	private string $response;

	function SendSMS($username, $password, $options, $message, $inputgsmnumbers)
	{
		$dir = '';
		$this->username = $username;
		$this->password = $password;
		$this->sender = $options['sender'];
		$this->token = $options['token'] ? true : false;
		if ($options['type_sms'] === 'simulate'){
			$dir = '/send/simulate'; // l'API accueillera la demande mais le SMS ne sera pas envoyé
		} else {
			$dir = '/send';
		}
		// definir un header pour accueillir le json et, si besoin, donner le token
		$headers = ['Content-Type: application/json', 'Accept: application/json'];
		if ($options['token']){
			$headers = array_merge($headers,["Authorization: Bearer " . $options['token']]);
		}
		$this->optional_headers = $headers;
		$this->message = $message;
		$this->inputgsmnumbers = $inputgsmnumbers;
		$this->host = "https://api.smsfactor.com" . $dir;
		$this->formatGSM();
		$this->formatMessage();

		// un petit log avec les informations essentielles pour le debug
		spip_log('host = ' . $this->host . ' request_data = ' . $this->request_data . ' headers = ' . print_r($this->optional_headers,true), 'sms' . _LOG_DEBUG);
		// on envoie le SMS
		$this->response = $this->do_post_request($this->host, $this->request_data, $this->optional_headers);
		return $this->response;
	}

	private function formatGSM(): void
	{
		foreach ($this->inputgsmnumbers as $cle => $n) {
			$fomatedGSM[] = ['value' => $n];
		}
		$this->fomatedGSM = $fomatedGSM;
	}

	private function formatMessage(): void
	{
		$formated = [
			'sms' => [
				'message' =>
					array_merge(['sender' => $this->sender], $this->texte_avec_liens_courts()),
				'recipients' => [
					'gsm' => $this->fomatedGSM
				]
			]
		];
		// s'il n'y a pas de token, on transmet le login et mot de passe
		if ($this->token == FALSE){
			$formated['sms']['authentification'] = [
				'username' => $this->username,
				'password' => $this->password
			];
		}
		$this->formatedMessage = $formated;
		$this->request_data =  json_encode($this->formatedMessage);
	}

	private function do_post_request($url, $postdata, $optional_headers = null)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $optional_headers);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;

	}

/**
 * Méthode privée pour permettre l'usage de liens courts
 *
 * Il faut identifier les liens dans le texte et les substituer par le tag prévu ```<-short->```,
 * puis indiquer à l'API les liens devant être raccourcis par un tableau links.
 * L'API les calculera et les placera à la place du tag ```<-short->```.
 *
 * @link https://dev.smsfactor.com/fr/api/sms/envoi/liens-courts
 *
 * @uses string  $this->message
 *                       Le texte du SMS
 * @return array  $message
 *                       Un nouveau formatage du texte en tableau 'text' s'il y a des liens 'links'
 */
	private function texte_avec_liens_courts() {
		// Masque
		$regex = '@([http|ftp|https]+://[a-z0-9?=:&\./+,%#_-]+)@i';
		$links = [];
		$text = $this->message;
		preg_match_all($regex, $text, $trouvaille, PREG_SET_ORDER);
		if ($trouvaille){
			foreach ($trouvaille as $key => $value) {
				$links[] = $value[0];
				$text = str_replace($value[0], '<-short->', $text);
			}
			return ['text' => $text, 'links' => $links];
		}
		return ['text' => $text];
	}
}
