#Plugin SMS

## Configuration
Dans la configuration du plugin, il faut renseigner les informations de connection de votre fournisseur.


##Appeler la fonction

1. via la fonction `envoyer_sms()`
```php
// definition des variables
$message = "du texte";
$dest = ['0611223344','0700700700'];
$options = ['sender' => 'SMS de SPIP'];

// Appel de la fonction
$retour = envoyer_sms($message,$dest,$options);
if ( $retour ){
	echo "message envoyé";
} else {
	echo "Erreur lors de l'envoi du message";
}
```

2. via `charger_fonction()`
```php
$envoyer_sms = charger_fonction('envoyer_sms', 'inc');
$envoyer_sms($message, $dest, $options);
```

3. via `charger_fonction()`
```php
$id_job = job_queue_add(
	'envoyer_sms',
	$description,
	array($message, $dest, $options),
	'inc/',
	true,
	$time
);
```
