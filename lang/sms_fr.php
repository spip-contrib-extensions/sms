<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/sms.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun' => 'Aucun',

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	'choix_prestataire' => 'Choix du prestataire',
	'cle_api' => 'Clé API',

	// D
	'date_debut' => 'Date de début',
	'date_fin' => 'Date de fin',
	'demo_envoyer' => 'Envoyer',
	'demo_erreur_test_via_appel_charger_fonction' => 'Le message n’a pas pu être envoyé en chargeant la fonction',
	'demo_erreur_test_via_appel_direct' => 'Le message n’a pas pu être envoyé en appelant directement la fonction',
	'demo_message' => 'Message',
	'demo_mode_charger' => 'Appel de la fonction <code>envoyer_sms()</code> avec <code>charger_fonction()</code>',
	'demo_mode_direct' => 'Appel direct de la fonction <code>envoyer_sms()</code>',
	'demo_numero' => 'Numéro',
	'demo_select_mode' => 'Selectionnez le mode d’appel de la fonction',
	'demo_sms_test' => 'Message de test',
	'demo_smsfactor_simulate' => 'Simuler l’envoi d’un SMS (active l’API mais n’envoie pas le SMS et ne vous coute rien)',
	'demo_test_ok_via_appel_charger_fonction' => 'Le message a bien été envoyé en chargeant la fonction',
	'demo_test_ok_via_appel_direct' => 'Le message a bien été envoyé en appelant directement la fonction',
	'demo_teste' => 'Page de test de l’envoi de SMS',

	// E
	'expediteur' => 'Expéditeur',
	'explication_expediteur' => 'Personnalisation de l’expéditeur avec du texte (11 caractères Max.) N° de Téléphone non pris en charge en France.',
	'explication_mode_octopush' => 'Souhaitez-vous envoyer en réel ou en simulation ?',
	'explication_octopush' => 'Identifiants API (Votre solde : SMS Standard @standard@ ou SMS premium @premium@)',
	'explication_smsfactor' => 'Identifiant de votre compte en ligne',
	'explication_token' => 'Un Token est un identifiant unique à la plateforme remplaçant le couple login / mot de passe qui vous permet de vous authentifier à l’API sans transmettre ces informations.',

	// L
	'login' => 'Login',

	// M
	'mode_octopush' => 'Mode d’envoi des sms',
	'mot_de_passe' => 'Mot de passe',

	// R
	'recup_nbr_sms' => 'Nombre de SMS',
	'reel' => 'Réel',

	// S
	'simulation' => 'Simulation',
	'sms' => 'SMS',
	'sms_titre' => 'SMS SPIP',

	// T
	'titre_page_configurer_sms' => 'Configuration de l’API SMS',
	'token_smsfactor' => 'Token SMSFactor'
);
