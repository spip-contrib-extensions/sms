<?php
/**
 * fonctions, variables et constantes nécessaires à l’espace privé et
 * utiles au plugin SMS SPIP
 *
 * @plugin     SMS SPIP
 * @copyright  2015
 * @author     tofulm
 * @licence    GNU/GPL
 * @package    SPIP\Sms\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

defined('_TEST_SMS_DEST') || define('_TEST_SMS_DEST', false);
