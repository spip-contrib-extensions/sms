<?php
/**
 * Fonctions utiles au plugin SMS SPIP
 *
 * @plugin	   SMS SPIP
 * @copyright  2015
 * @author	   tofulm
 * @licence    GNU/GPL
 * @package    SPIP\Sms\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

function envoyer_sms($message, $destinataire, $arg=array()) {
	$envoyer_sms = charger_fonction('envoyer_sms', 'inc');
	return $envoyer_sms($message, $destinataire, $arg);
}
/**
 * Afficher la balance de sms disponible
 *
 * <INCLURE{fond=inclure/octopush_balance} /> pour l'affichage
 *
 * @param string $type
 *		standard ou premium
 * @return boolean
 * 		valeur en nombre entier de sms restant
**/
function filtre_balance($type) {
	$username = lire_config('sms/login_octopush');
	$password = lire_config('sms/cle_api_octopush');

	require_once('classes/octopush/sms.inc.php');
	$sms = new SMS_OCTOSPUSH();

	$sms->set_user_login($username);
	$sms->set_api_key($password);

	$xml = $sms->getBalance();
	$xml = simplexml_load_string($xml);
	$balance = $xml->balance;
	$standard = $balance['1'];
	$premium = $balance['0'];
	$balance = array('standard' => $standard, 'premium' => $premium);
	$valeurs = intval($balance[$type]);
	return $valeurs;
}

function nettoyer_xml($texte) {
	$texte = str_replace('&', '&amp;',  $texte);
	$texte = str_replace('<', '&lt;',   $texte);
	$texte = str_replace('>', '&gt;',   $texte);
	$texte = str_replace('"', '&quot;', $texte);
	$texte = str_replace("'", "&apos;", $texte);

	return $texte;
}
